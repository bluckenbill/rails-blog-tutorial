class MakeArticleFieldsNonNull < ActiveRecord::Migration[5.2]

  def up
    change_column_null :articles, :text, false
    change_column_null :articles, :title, false
  end

  def down
    change_column_null :articles, :text, true
    change_column_null :articles, :title, true
  end
end
